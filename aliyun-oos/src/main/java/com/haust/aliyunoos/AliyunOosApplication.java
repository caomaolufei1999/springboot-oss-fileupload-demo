package com.haust.aliyunoos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AliyunOosApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliyunOosApplication.class, args);
    }

}
